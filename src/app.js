import { initialize } from './initializers/index.js';
import { log } from './utils/debug.js';
import { inject } from './services/index.js';

const initializers = [
    'set-app-name',
    'test',
    'set-document-title',
];

class App {
    router = inject('router');

    _runInitializers() {
        log('// Run initializers');
        return Promise.all([
            initializers.map(name => initialize(name, this))
        ]);
    }

    async init() {
        log('App is booting');

        // Run initializers
        await this._runInitializers();

        await this._initServices();
    }
}

export default App;
