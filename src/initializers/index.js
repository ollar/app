export async function initialize(name, app) {
    const { default: func } = await import(`./${name}.js`);
    return func(app);
}