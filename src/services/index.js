import AllServices from './_list.js';

const services = {};

export function initServices() {
    return Promise.all(Object.keys(AllServices).map(_service =>
        import(`./${_service}.js`).then(({ default: Service }) => {
            services[name] = new Service();
        }))
    );
}

export function inject(name) {
    return lookup.bind(null, name);
}

export function lookup(name) {
    return services[name];
}
